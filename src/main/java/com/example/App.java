package com.example;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.apache.commons.lang3.StringUtils;
import java.util.Scanner;

public class App {
    private static final Logger logger = LogManager.getLogger(App.class);

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Get user input
        System.out.println("Enter your username:");
        String username = scanner.nextLine();
        System.out.println("Enter a message:");
        String message = scanner.nextLine();

        // Vulnerable function call
        ThreadContext.put("username", username);
        
        // Logging the user input
        logger.info("User login: {}", username);
        logger.info("User message: {}", message);

        // Using a safe library
        String safeOutput = StringUtils.capitalize(message);
        System.out.println("Capitalized message: " + safeOutput);
        
        scanner.close();
    }
}


